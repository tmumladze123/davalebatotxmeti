package com.example.datastore.Fragments

import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.fragment.app.viewModels
import androidx.lifecycle.LiveData
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.datastore.Person
import com.example.datastore.R
import com.example.datastore.ViewModel.UserViewModel
import com.example.datastore.databinding.FragmentProfileBinding
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class ProfileFragment : BaseFragment<FragmentProfileBinding>(FragmentProfileBinding::inflate) {
    var data = Person()
    val manager= context?.let { manager(it) }
    override fun start() {
        btnListener()
        observes()
    }

    fun btnListener() {
        binding.btnEditProfilePicture.setOnClickListener {
            changeInfo()
        }
    }
    fun observes() {
        viewLifecycleOwner.lifecycleScope.launch {
            withContext(IO)
            {
                manager!!.person.observe(viewLifecycleOwner,{
                    binding.tvFirstName.text= it.firstName
                    binding.tvLastName.text= it.lastName
                   // binding.tvAddress.text= it.address
                    binding.tvAge.text= it.age
                    binding.tvPhoneNum.text= it.phoneNumber
                    binding.tvGender.text= it.gender
                    binding.tvEmail.text= it.email
                })
            }
        }

    }
    fun changeInfo() {

        findNavController().navigate(R.id.action_profileFragment_to_editProfileFragment)

    }

}


