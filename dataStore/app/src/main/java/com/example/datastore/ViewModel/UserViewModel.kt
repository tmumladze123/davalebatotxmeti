package com.example.datastore.ViewModel


import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.datastore.Person

class UserViewModel : ViewModel() {
    private  var _personInfo = MutableLiveData<Person>()
    val personInfo: LiveData<Person>
        get() = _personInfo
    private var person=Person()
    fun setData()
    {
        _personInfo.value=person
    }

}