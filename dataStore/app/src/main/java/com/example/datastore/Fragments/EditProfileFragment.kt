package com.example.datastore.Fragments


import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.lifecycle.lifecycleScope
import com.example.datastore.Person
import com.example.datastore.databinding.FragmentEditProfileFragmentBinding
import kotlinx.coroutines.launch


class EditProfileFragment :  BaseFragment<FragmentEditProfileFragmentBinding>(FragmentEditProfileFragmentBinding::inflate) {
    val firstName = stringPreferencesKey("FirstName")
    val lastName = stringPreferencesKey("LastName")
    val address = stringPreferencesKey("address")
    val age = stringPreferencesKey("Age")
    val email = stringPreferencesKey("Email")
    val gender = stringPreferencesKey("Gender")
    val phoneNumber = stringPreferencesKey("PhoneNumber")
    val fieldData=Person()
    val manager= context?.let { manager(it) }
    override fun start() {
    listeners()
    }
    fun listeners()
    {
        binding.btnEdit.setOnClickListener {
            getDataFromField()
            viewLifecycleOwner.lifecycleScope.launch{
                changeData()
            }
        }
    }
    suspend fun changeData()
    {
        getDataFromField()
        manager!!.saveToDataStore(fieldData)

    }
    fun getDataFromField()
    {
        if(binding.etLastName.text?.isEmpty() == true) {
            fieldData.lastName=binding.etLastName.text.toString()
        }
        if(binding.etFirstName.text?.isEmpty() == true) {
            fieldData.firstName=binding.etFirstName.text.toString()
        }
        if(binding.edAddress.text?.isEmpty() == true) {
            fieldData.address=binding.edAddress.text.toString()
        }
        if(binding.etAge.text?.isEmpty() == true) {
            fieldData.age=binding.etAge.text.toString()
        }
        if(binding.etEmail.text?.isEmpty() == true) {
            fieldData.email=binding.etEmail.text.toString()
        }
        if(binding.etGender.text?.isEmpty() == true) {
            fieldData.gender=binding.etGender.text.toString()
        }
        if(binding.edPhoneNum.text?.isEmpty() == true) {
            fieldData.phoneNumber=binding.edPhoneNum.text.toString()
        }

    }
}