package com.example.datastore

import androidx.datastore.preferences.core.Preferences

data class Person(
    var firstName:String="Tamari",
    var lastName:String="Mumladze",
    var age:String="21",
    var email:String="email",
    var profileImage:String="bla",
    var gender:String="Female",
    var address: String ="HomeLess",
    var phoneNumber:String="XOXOXO"
)
