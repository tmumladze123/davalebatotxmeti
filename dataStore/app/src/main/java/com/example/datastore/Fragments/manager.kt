package com.example.datastore.Fragments

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import androidx.lifecycle.LiveData
import kotlinx.coroutines.flow.map

val Context.dataStore: DataStore<Preferences> by preferencesDataStore("person")

class manager(context: Context) {
    val dataStore: DataStore<Preferences> = context.dataStore
    val firstName = stringPreferencesKey("FirstName")
    val lastName = stringPreferencesKey("LastName")
    val address = stringPreferencesKey("address")
    val age = stringPreferencesKey("Age")
    val email = stringPreferencesKey("Email")
    val gender = stringPreferencesKey("Gender")
    val phoneNumber = stringPreferencesKey("PhoneNumber")
    suspend fun edit() {

    }

    suspend fun saveToDataStore(person: com.example.datastore.Person) {
        dataStore.edit {
            it[firstName] = person.firstName
            it[lastName] = person.lastName
            it[address] = person.address
            it[age] = person.age
            it[email] = person.email
            it[gender] = person.gender
            it[phoneNumber] = person.phoneNumber
        }
    }
    var person: LiveData<com.example.datastore.Person> =dataStore.data.map {
        com.example.datastore.Person(
            it[firstName].toString(),
            it[lastName].toString(),
            it[address].toString(),
            it[age].toString(),
            it[email].toString(),
            it[gender].toString())
            it[phoneNumber].toString()
    } as LiveData<com.example.datastore.Person>


}